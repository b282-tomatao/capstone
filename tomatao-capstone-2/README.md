# Backend Server Project - E-Commerce App

Welcome to the backend server project developed during the second part of the Zuitt Coding Bootcamp's main course. This project focuses exclusively on building a robust backend server for an e-commerce application. Please note that this project doesn't include a user interface, as it is centered around the server-side functionality.

## Project Overview

As a result of the second part of our main course in the Zuitt Coding Bootcamp, this project involves the creation of a fully functional backend server. The server is designed to support an e-commerce app and is built using the following frameworks: Node.js, Express.js, and MongoDB. The server's code repository is hosted on GitLab, and the deployment is handled through Render.

## Skills Utilized

Throughout the development of this project, I've employed a range of skills and technologies, including:

- JavaScript
- Node.js
- Express.js
- NoSQL
- MongoDB
- REST API
- Database Design
- Postman
- Robo3T/Studio3T
- Render
- Back-End Web Development

Feel free to explore the project's codebase to gain insights into its implementation details and the technologies involved.

---

This project was created as a component of the Zuitt Coding Bootcamp's main course, focusing on backend server development. It serves as a demonstration of the skills and expertise gained throughout this phase of the program.

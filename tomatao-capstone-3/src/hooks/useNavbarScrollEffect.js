import { useEffect } from 'react';

const useNavbarScrollEffect = (navbarScrollOffset) => {
  const updateNavColor = (entries) => {
    const [entry] = entries;
    const nav = document.getElementById('navbar');
    const nav2 = document.getElementById('navbar2');

    if (!entry.isIntersecting) {
      nav.classList.add("fixed-top-transparent-scrolled");
      nav.classList.remove("fixed-top-transparent");
    } else {
      nav.classList.add("fixed-top-transparent");
      nav.classList.remove("fixed-top-transparent-scrolled");
    }

    if (!entry.isIntersecting) {
      nav2.classList.add("fixed-top-transparent-scrolled");
      nav2.classList.remove("fixed-top-transparent");
    } else {
      nav2.classList.add("fixed-top-transparent");
      nav2.classList.remove("fixed-top-transparent-scrolled");
    }
  };

  useEffect(() => {
    const nav = document.getElementById('navbar');
    const nav2 = document.getElementById('navbar2');

    nav.classList.remove("fixed-top-transparent-scrolled");
    nav.classList.add("fixed-top-transparent");

    nav2.classList.remove("fixed-top-transparent-scrolled");
    nav2.classList.add("fixed-top-transparent");

    const scrollOffsetIndicator = document.querySelector('.scrollOffsetIndicator');

    if (typeof IntersectionObserver !== 'undefined' && scrollOffsetIndicator !== null) {
      const headerObserver = new IntersectionObserver(updateNavColor, {
        root: null,
        threshold: 0,
        rootMargin: `-${navbarScrollOffset}px`,
      });

      headerObserver.observe(scrollOffsetIndicator);

      return () => {
        headerObserver.unobserve(scrollOffsetIndicator);
      };
    }
  }, [navbarScrollOffset]);
};

export default useNavbarScrollEffect;

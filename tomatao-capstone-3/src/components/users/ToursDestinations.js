import ToursMain from "./ToursMain";
import ToursMobileAccordion from "./ToursMobileAccordion";
import useFetch from "../../hooks/useFetch";
import { useContext } from "react";
import { AllContext } from "../../context/AllContext";

const ToursDestinations = () => {

    useFetch(`${process.env.REACT_APP_API_URL}/users/admin/bookings`, 'SET_BOOKING');
    const { bookings } = useContext(AllContext);

    return ( 
        <div className="row destinations">
            <h3 className="d-lg-none">Destinations:</h3>
            {/* Destinations Accordion (phone screen) */}
            {
                !bookings ?
                <img src="/images/loading.gif" alt="loading" />
                :
                <ToursMobileAccordion />
            }
            {/* LARGE SCREEN DESTINATIONS AND BOOKINGS */}
            <ToursMain />
        </div>
    );
}
 
export default ToursDestinations;
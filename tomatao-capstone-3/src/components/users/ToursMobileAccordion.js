import { Accordion } from "react-bootstrap";
import { useContext, useState } from "react";
import { AllContext } from "../../context/AllContext";
import useReformatDate from "../../hooks/useReformatDate";

const ToursMobileAccordion = () => {

    const { reformatDate } = useReformatDate()
    const [fetchedTourists, setFetchedTourists] = useState(null);
    const { bookings, token } = useContext(AllContext);

    if (bookings) {

        const allDestinationBookings = bookings.reduce((acc, booking) => {
            const destination = booking.destination;
            
            // Check if a destination group already exists in the accumulator
            const existingGroup = acc.find(group => group.destination === destination);
            
            if (existingGroup) {
                // If the destination group exists, push the current booking to it
                existingGroup.destinationBookings.push(booking);
            } else {
                // If the destination group doesn't exist, create a new one
                acc.push({
                destination: destination,
                destinationBookings: [booking]
                });
            }
                return acc;
        }, []);
        
        const destination = allDestinationBookings.map((destinationBooking) => {
            const { destination, destinationBookings } = destinationBooking;

            const bookings = destinationBookings.map((booking) => {
                const {
                    _id,
                    packageDuration,
                    tourStarts,
                    tourEnds,
                    travelPlan
                } = booking

                const dates = reformatDate(tourStarts, tourEnds)

                const fetchWithId = (_id) => {

                    fetch(`${process.env.REACT_APP_API_URL}/users/admin/bookings/${_id}`, {
                        headers: {'Authorization': `Bearer ${token}`}
                    })
                    .then(res => {
                        if (!res.ok) {
                            throw Error('There seems to be a problem fetching data.');
                        } else {
                            return res.json();
                        };  
                    })
                    .then(data => {
                        setFetchedTourists(data)
                        return data
                    })
                    .catch(err => {
                        if (err.name === 'AbortError') {
                            console.log(err);
                        }
                    })
                };

                let tourists;
                let touristCount = 0;
                
                if (fetchedTourists) {
                    tourists = fetchedTourists.map((tourist, i) => {
                        const { username, fullname, buddies } = tourist;
                        touristCount++
                        let userBuddies;
                        if (buddies && buddies.length !== 0) {
                            userBuddies = buddies.map((buddy, i) => {
                                
                                if (buddy.length !== 0) {
                                    touristCount++
                                }
                                return (
                                    <li key={i}>{buddy}</li>
                                )
                            });
                        };

                        return (
                            <li key={i}>
                                <p style={{margin: '0'}}>
                                    {username}
                                </p>
                                <p>
                                    {`(${fullname})`}
                                </p>
                                {
                                    buddies && buddies[0].length !== 0 &&
                                    <>
                                    <p style={{margin: '0'}}>
                                        Buddies:
                                    </p>
                                    <ul>
                                        { userBuddies }
                                    </ul>
                                    <br />
                                    </>
                                }
                            </li>
                        )
                    })
                }


                return (
                    <Accordion.Item
                        key={_id}
                        eventKey={_id}
                        onClick={() => {fetchWithId(_id); setFetchedTourists(null)}}
                        className="destination-bookings destinations-list-item"
                    >
                        <Accordion.Header>
                            { dates } | { packageDuration } { travelPlan }
                        </Accordion.Header>
                        <Accordion.Body className="">
                            <h3>{`Tourists: (${touristCount})`}</h3>
                            <ol>
                                {tourists}
                            </ol>
                        </Accordion.Body>
                    </Accordion.Item>
                )
            });

            return ( 
                <Accordion.Item  key={destination} eventKey={destination} className="destinations-list-item">
                    <Accordion.Header>
                        { destination }
                    </Accordion.Header>
                    <Accordion.Body>
                            <h3>Bookings:</h3>
                            <Accordion>
                                { bookings }
                            </Accordion>
                    </Accordion.Body>
                </Accordion.Item>
            );
        })

        return (
            <Accordion className="d-lg-none">
                { destination }
            </Accordion>
        )
    }
    
}
 
export default ToursMobileAccordion;
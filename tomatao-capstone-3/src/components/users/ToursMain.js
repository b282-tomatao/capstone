import { Accordion } from "react-bootstrap";
import { useContext } from "react";
import { AllContext } from "../../context/AllContext";
import AdminBookings from "./AdminBookings";
import AdminTourPackages from "./AdminTourPackages";
import { TopDestination } from "./AdminModify";

const ToursMain = () => {

    const { bookings, tourPackages } = useContext(AllContext);

    if (bookings && tourPackages) {

        let allDestinationBookings;

        if (!bookings.error) {
            
            allDestinationBookings = bookings.reduce((acc, booking) => {
                const destination = booking.destination;
                
                // Check if a destination group already exists in the accumulator
                const existingGroup = acc.find(group => group.destination === destination);
                
                if (existingGroup) {
                    // If the destination group exists, push the current booking to it
                    existingGroup.destinationBookings.push(booking);
                } else {
                    // If the destination group doesn't exist, create a new one
                    acc.push({
                    destination: destination,
                    destinationBookings: [booking]
                    });
                }
                    return acc;
            }, []);
        };

        const allDestinationsTourPackages = tourPackages.reduce((acc, tourPackage) => {
            const destination = tourPackage.destination;
            
            // Check if a destination group already exists in the accumulator
            const existingGroup = acc.find(group => group.destination === destination);
            
            if (existingGroup) {
                // If the destination group exists, push the current booking to it
                existingGroup.destinationTourPackages.push(tourPackage);
            } else {
                // If the destination group doesn't exist, create a new one
                acc.push({
                destination: destination,
                destinationTourPackages: [tourPackage]
                });
            }

            const toAlphabet = acc.sort((a, b) => a.destination.localeCompare(b.destination));

            return toAlphabet;

        }, []);

        let destinationTourPackages;
        let allBookings;
        const destination = allDestinationsTourPackages.map((tourPackage) => {
            let destination = tourPackage.destination
            destinationTourPackages = tourPackage.destinationTourPackages

            const isTopDestination = destinationTourPackages[0].isTopDestination

            let bookingsFromDestination;
            
            if (!bookings.error) {
                bookingsFromDestination = allDestinationBookings.filter(bookings => {
                    return bookings.destination === destination;
                });


                allBookings = bookingsFromDestination.map(bookings => {
                    return bookings.destinationBookings;
                });
            }

            return ( 
                <li key={destination} >
                    <p>{ destination }</p>
                </li>
            );
        })

        return ( 
            <div className="destination-and-bookings row d-none d-lg-flex">
                {/* DESTINATION ITEMS */}
                <div className="lg-destination-items col-5">
                    <h3>Destinations:</h3>
                    <ul>
                        { destination }
                    </ul>
                </div>
    
                {/* BOOKINGS */}
                <div className="lg-bookings col-7">
                    <h3>Tour Packages:</h3>
                    <Accordion>
                        <AdminTourPackages destinationTourPackages={destinationTourPackages} allBookings={allBookings} />
                    </Accordion><br />
                    <h3>Bookings:</h3>
                    <Accordion>
                        <AdminBookings allBookings={allBookings} />
                    </Accordion>
                    
                </div>
                {/* BOOKINGS END */}
            </div>
        );
    }
}
 
export default ToursMain;